package com.swifty.itunestopmusic.toplist.model.local;

import android.support.annotation.NonNull;

import com.orm.SugarRecord;
import com.swifty.itunestopmusic.base.PerActivity;
import com.swifty.itunestopmusic.toplist.model.IDataHolder;
import com.swifty.itunestopmusic.toplist.model.MusicBean;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by swifty on 20/11/2017.
 */
@PerActivity
public class LocalDataHolder implements IDataHolder {

    @Inject
    public LocalDataHolder() {
    }

    @Override
    @NonNull
    public Observable<List<MusicBean>> musicList() {
        Observable<List<MusicBean>> musicObservable = Observable.create((ObservableOnSubscribe<List<MusicBean>>) e -> {
            List<MusicBean> musicBeans = SugarRecord.listAll(MusicBean.class);
            e.onNext(musicBeans);
            e.onComplete();
        }).subscribeOn(Schedulers.io());
        return musicObservable;
    }

    @NonNull
    public Observable<Boolean> saveMusicList(List<MusicBean> musicBeans) {
        Observable<Boolean> booleanObservable = Observable.create((ObservableOnSubscribe<Boolean>) e -> {
            SugarRecord.updateInTx(musicBeans);
            e.onNext(true);
            e.onComplete();
        }).subscribeOn(Schedulers.io());
        return booleanObservable;
    }
}
