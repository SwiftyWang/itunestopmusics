package com.swifty.itunestopmusic.toplist.model.network;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by swifty on 20/11/2017.
 */

public interface TopMusicListService {

    @GET("api/v1/us/apple-music/top-songs/all/25/explicit.json")
    Observable<MusicListResponse> topMusicList();
}
