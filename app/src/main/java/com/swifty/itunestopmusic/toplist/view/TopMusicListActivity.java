package com.swifty.itunestopmusic.toplist.view;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.swifty.itunestopmusic.MainApplication;
import com.swifty.itunestopmusic.R;
import com.swifty.itunestopmusic.toplist.DaggerTopListComponent;
import com.swifty.itunestopmusic.toplist.TopListContract;
import com.swifty.itunestopmusic.toplist.TopListModule;
import com.swifty.itunestopmusic.toplist.model.MusicBean;
import com.swifty.itunestopmusic.toplist.presenter.TopListPresenter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class TopMusicListActivity extends AppCompatActivity implements TopListContract.View {

    @BindView(R.id.refresh) SwipeRefreshLayout refresh;
    @BindView(R.id.container) RecyclerView container;
    @Inject TopListPresenter topListPresenter;
    private MenuItem item;
    private TopMusicListAdapter topMusicListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        DaggerTopListComponent.builder()
                .applicationComponent(((MainApplication) getApplicationContext()).getComponent())
                .topListModule(new TopListModule(this))
                .build()
                .inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initViews();
    }

    private void initViews() {
        topMusicListAdapter = new TopMusicListAdapter(this);
        refresh.setOnRefreshListener(this::refresh);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,
                linearLayoutManager.getOrientation());
        container.addItemDecoration(dividerItemDecoration);
        container.setLayoutManager(linearLayoutManager);
        container.setAdapter(topMusicListAdapter);
        topListPresenter.emitFetchDataLocal();
    }

    @Override
    public void updateRefreshButton(Boolean enable) {
        if (enable == null || item == null) {
            return;
        }
        if (enable) {
            item.setEnabled(true);
            item.getIcon().setAlpha(255);
        } else {
            item.setEnabled(false);
            item.getIcon().setAlpha(130);
        }
    }

    @Override
    public void renderView(List<MusicBean> musicBeans) {
        refresh.setRefreshing(false);
        if (musicBeans == null) {
            Timber.w("musicBeans is null");
            Snackbar.make(container, getString(R.string.show_list_error), Snackbar.LENGTH_SHORT).show();
            return;
        }
        topMusicListAdapter.updateList(musicBeans);
    }

    @Override
    public void refresh() {
        if (!refresh.isRefreshing()) {
            refresh.setRefreshing(true);
        }
        topListPresenter.emitFetchDataRemote();
    }

    @Override
    protected void onResume() {
        super.onResume();
        topListPresenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        topListPresenter.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        item = menu.findItem(R.id.refresh);
        updateRefreshButton(topListPresenter.isNetworkEnabled());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh:
                Timber.d("refresh click");
                refresh();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
