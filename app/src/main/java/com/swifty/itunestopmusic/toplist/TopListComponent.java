package com.swifty.itunestopmusic.toplist;

import com.swifty.itunestopmusic.ApplicationComponent;
import com.swifty.itunestopmusic.base.PerActivity;
import com.swifty.itunestopmusic.toplist.view.TopMusicListActivity;

import dagger.Component;

/**
 * Created by swifty on 20/11/2017.
 */
@PerActivity
@Component(modules = TopListModule.class, dependencies = ApplicationComponent.class)
public interface TopListComponent {
    void inject(TopMusicListActivity topMusicListActivity);
}
