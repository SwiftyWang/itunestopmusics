package com.swifty.itunestopmusic.toplist;

import android.content.Context;

import com.swifty.itunestopmusic.base.PerActivity;
import com.swifty.itunestopmusic.common.NetworkHelper;
import com.swifty.itunestopmusic.toplist.model.local.LocalDataHolder;
import com.swifty.itunestopmusic.toplist.model.network.NetworkDataHolder;
import com.swifty.itunestopmusic.toplist.presenter.TopListPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by swifty on 20/11/2017.
 */
@Module
public class TopListModule {
    private TopListContract.View view;

    public TopListModule(TopListContract.View view) {
        this.view = view;
    }

    @PerActivity
    @Provides
    TopListContract.View mainContractView() {
        return view;
    }

    @PerActivity
    @Provides
    TopListPresenter mainPresenter(Context context, NetworkHelper networkHelper, TopListContract.View view, NetworkDataHolder fetchDataFromNetwork, LocalDataHolder fetchDataFromCache) {
        return new TopListPresenter(context, networkHelper, view, fetchDataFromNetwork, fetchDataFromCache);
    }
}
