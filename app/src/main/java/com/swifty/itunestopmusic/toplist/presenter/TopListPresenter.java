package com.swifty.itunestopmusic.toplist.presenter;

import android.content.Context;

import com.swifty.itunestopmusic.base.PerActivity;
import com.swifty.itunestopmusic.common.NetworkHelper;
import com.swifty.itunestopmusic.toplist.TopListContract;
import com.swifty.itunestopmusic.toplist.model.MusicBean;
import com.swifty.itunestopmusic.toplist.model.local.LocalDataHolder;
import com.swifty.itunestopmusic.toplist.model.network.NetworkDataHolder;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by swifty on 20/11/2017.
 */

public class TopListPresenter implements TopListContract.Presenter {

    private final Context context;
    private final LocalDataHolder fetchDataFromCache;
    private final NetworkDataHolder fetchDataFromNetwork;
    private final NetworkHelper networkHelper;
    private final TopListContract.View view;
    private Disposable fetchDataLocal;
    private Disposable fetchDataRemote;
    private Disposable merged;

    @PerActivity
    public TopListPresenter(Context context, NetworkHelper networkHelper, TopListContract.View view, NetworkDataHolder fetchDataFromNetwork, LocalDataHolder fetchDataFromCache) {
        this.context = context;
        this.view = view;
        this.networkHelper = networkHelper;
        this.fetchDataFromNetwork = fetchDataFromNetwork;
        this.fetchDataFromCache = fetchDataFromCache;
    }

    @Override
    public void emitFetchData() {
        if (merged == null || merged.isDisposed()) {
            Timber.d("emitFetchData");
            Observable<List<MusicBean>> serverObv = fetchDataFromNetwork.musicList().share();
            Observable<List<MusicBean>> localObv = fetchDataFromCache
                    .musicList()
                    .takeUntil(serverObv);
            merged = Observable.merge(localObv, serverObv)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext(musicBeans -> fetchDataFromCache.saveMusicList(musicBeans).subscribe(ret -> {
                        Timber.d("save db result " + ret);
                    }))
                    .subscribe(view::renderView,
                            throwable -> {
                                Timber.e(throwable);
                                view.renderView(null);
                            });
        }
    }

    @Override
    public void emitFetchDataLocal() {
        if (fetchDataLocal == null || fetchDataLocal.isDisposed()) {
            Timber.d("emitFetchDataLocal");
            fetchDataLocal = fetchDataFromCache.musicList()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(view::renderView,
                            throwable -> {
                                Timber.e(throwable);
                                view.renderView(null);
                            });
        }
    }

    @Override
    public void emitFetchDataRemote() {
        if (fetchDataRemote == null || fetchDataRemote.isDisposed()) {
            Timber.d("emitFetchDataRemote");
            fetchDataRemote = fetchDataFromNetwork.musicList()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext(musicBeans -> fetchDataFromCache.saveMusicList(musicBeans).subscribe(ret -> {
                        Timber.d("save db result" + ret);
                    }))
                    .subscribe(view::renderView,
                            throwable -> {
                                Timber.e(throwable);
                                view.renderView(null);
                            });
        }
    }

    @Override
    public boolean isNetworkEnabled() {
        return networkHelper.isNetworkAvailable();
    }

    @Override
    public void networkChange() {
        networkHelper.networkStatus()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(view::updateRefreshButton);
    }

    @Override
    public void onPause() {
        networkHelper.unRegister();
    }

    @Override
    public void onResume() {
        networkHelper.register();
        networkChange();
    }
}
