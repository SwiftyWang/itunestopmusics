package com.swifty.itunestopmusic.toplist.model;

import android.support.annotation.NonNull;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by swifty on 20/11/2017.
 */

public interface IDataHolder {

    @NonNull
    Observable<List<MusicBean>> musicList();
}
