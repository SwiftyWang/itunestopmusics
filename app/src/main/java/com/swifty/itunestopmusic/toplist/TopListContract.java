package com.swifty.itunestopmusic.toplist;

import com.swifty.itunestopmusic.base.BasePresenter;
import com.swifty.itunestopmusic.base.BaseView;
import com.swifty.itunestopmusic.toplist.model.MusicBean;

import java.util.List;

/**
 * Created by swifty on 20/11/2017.
 */

public interface TopListContract {
    interface Presenter extends BasePresenter {
        void emitFetchData();

        void emitFetchDataLocal();

        void emitFetchDataRemote();

        boolean isNetworkEnabled();

        void networkChange();

        void onPause();

        void onResume();
    }

    interface View extends BaseView<Presenter> {
        void refresh();

        void renderView(List<MusicBean> musicBeans);

        void updateRefreshButton(Boolean enable);
    }
}
