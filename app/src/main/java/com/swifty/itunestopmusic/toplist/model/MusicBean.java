package com.swifty.itunestopmusic.toplist.model;

import com.google.gson.annotations.SerializedName;
import com.orm.dsl.Table;
import com.orm.dsl.Unique;

import java.util.List;

/**
 * Created by swifty on 20/11/2017.
 */
@Table
public class MusicBean {
    public String artistId;
    public String artistName;
    /**
     * artistUrl : https://itunes.apple.com/us/artist/post-malone/966309175?app=music
     * artistId : 966309175
     * artistName : Post Malone
     * artworkUrl100 : http://is2.mzstatic.com/image/thumb/Music118/v4/b1/b3/5a/b1b35a74-fa6e-e909-a2dd-61ff21631059/source/200x200bb.png
     * collectionName : rockstar (feat. 21 Savage) - Single
     * copyright : ℗ 2017 Republic Records, a division of UMG Recordings, Inc.
     * genres : [{"genreId":"18","name":"Hip-Hop/Rap","url":"https://itunes.apple.com/us/genre/id18"},{"genreId":"34","name":"Music","url":"https://itunes.apple.com/us/genre/id34"}]
     * id : 1281165480
     * kind : song
     * name : rockstar (feat. 21 Savage)
     * releaseDate : 2017-09-15
     * url : https://itunes.apple.com/us/album/rockstar-feat-21-savage/1281165478?i=1281165480&app=music
     */

    public String artistUrl;
    public String artworkUrl100;
    public String collectionName;
    public String copyright;
    public List<GenresBean> genres;
    public String kind;
    @Unique
    @SerializedName("id")
    public String musicId;
    public String name;
    public String releaseDate;
    public String url;

    public static class GenresBean {
        /**
         * genreId : 18
         * name : Hip-Hop/Rap
         * url : https://itunes.apple.com/us/genre/id18
         */

        public String genreId;
        public String name;
        public String url;
    }
}
