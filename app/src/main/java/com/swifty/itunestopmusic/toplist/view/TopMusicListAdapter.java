package com.swifty.itunestopmusic.toplist.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.swifty.itunestopmusic.R;
import com.swifty.itunestopmusic.toplist.model.MusicBean;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by swifty on 20/11/2017.
 */

public class TopMusicListAdapter extends RecyclerView.Adapter<TopMusicListAdapter.ViewHolder> {

    private final Context context;
    private final List<MusicBean> musicBeanList = new ArrayList<>();

    TopMusicListAdapter(@NonNull Context context) {
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder = new ViewHolder(LayoutInflater.from(context).inflate(R.layout.view_music_item, parent, false));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindData(musicBeanList.get(position));
    }

    @Override
    public int getItemCount() {
        return musicBeanList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.author_name) TextView authorName;
        @BindView(R.id.icon) ImageView icon;
        @BindView(R.id.song_name) TextView songName;
        @BindView(R.id.release_date) TextView releaseDate;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindData(MusicBean musicBean) {
            icon.setImageResource(0);
            Glide.with(context).load(musicBean.artworkUrl100)
                    .into(icon);
            authorName.setText(musicBean.artistName);
            songName.setText(musicBean.name);
            releaseDate.setText(musicBean.releaseDate);
        }
    }

    void updateList(List<MusicBean> musicBeanList) {
        if (musicBeanList == null) {
            return;
        }
        this.musicBeanList.clear();
        this.musicBeanList.addAll(musicBeanList);
        notifyDataSetChanged();
    }
}
