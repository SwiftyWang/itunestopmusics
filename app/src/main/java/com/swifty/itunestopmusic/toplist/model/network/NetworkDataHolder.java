package com.swifty.itunestopmusic.toplist.model.network;

import android.support.annotation.NonNull;

import com.swifty.itunestopmusic.toplist.model.IDataHolder;
import com.swifty.itunestopmusic.toplist.model.MusicBean;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import retrofit2.Retrofit;

/**
 * Created by swifty on 20/11/2017.
 */

public class NetworkDataHolder implements IDataHolder {

    private final Retrofit retrofit;

    @Inject
    public NetworkDataHolder(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    @Override
    @NonNull
    public Observable<List<MusicBean>> musicList() {
        return retrofit
                .create(TopMusicListService.class)
                .topMusicList()
                .map(musicListResponse -> musicListResponse.feed.results);
    }
}
