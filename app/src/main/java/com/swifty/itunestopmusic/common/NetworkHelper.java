package com.swifty.itunestopmusic.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by swifty on 20/11/2017.
 */
public class NetworkHelper {
    private final Context context;
    private final NetworkReceiver networkReceiver;
    private final PublishSubject<Boolean> statusPublish;

    public NetworkHelper(Context context) {
        this.context = context;
        this.networkReceiver = new NetworkReceiver();
        this.statusPublish = PublishSubject.create();
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null) {
            return false;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void register() {
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        context.registerReceiver(networkReceiver, intentFilter);
    }

    public void unRegister() {
        context.unregisterReceiver(networkReceiver);
    }

    public Observable<Boolean> networkStatus() {
        return statusPublish;
    }

    private class NetworkReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            statusPublish.onNext(isNetworkAvailable());
        }
    }
}
