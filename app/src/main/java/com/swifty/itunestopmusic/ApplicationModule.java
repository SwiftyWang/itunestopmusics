package com.swifty.itunestopmusic;

import android.content.Context;

import com.swifty.itunestopmusic.common.NetworkHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

/**
 * Created by swifty on 20/11/2017.
 */

@Module
public class ApplicationModule {
    private static final String BASE_URL = "https://rss.itunes.apple.com";
    private final Context context;

    public ApplicationModule(Context context) {
        this.context = context;
        Timber.plant(new Timber.DebugTree());
    }

    @Provides
    @Singleton
    Context context() {
        return context;
    }

    @Provides
    @Singleton
    Retrofit retrofit() {
        return retrofitBuilder()
                .baseUrl(BASE_URL)
                .build();
    }

    @Provides
    Retrofit.Builder retrofitBuilder() {
        Retrofit.Builder builder = new Retrofit.Builder();
        return builder
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create());
    }

    @Provides
    NetworkHelper networkHelper(Context context) {
        return new NetworkHelper(context);
    }
}
