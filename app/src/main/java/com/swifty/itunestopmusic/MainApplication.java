package com.swifty.itunestopmusic;

import android.app.Application;

import com.orm.SugarContext;

/**
 * Created by swifty on 20/11/2017.
 */

public class MainApplication extends Application {
    private ApplicationComponent component;

    public ApplicationComponent getComponent() {
        return component;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SugarContext.init(this);
        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        component.inject(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        SugarContext.terminate();
    }
}
