package com.swifty.itunestopmusic;

import android.content.Context;

import com.swifty.itunestopmusic.common.NetworkHelper;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

/**
 * Created by swifty on 20/11/2017.
 */
@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {
    void inject(MainApplication mainApplication);

    Retrofit getRetrofit();

    Context getContext();

    NetworkHelper getNetworkHelper();
}
